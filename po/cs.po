# Czech translation for lomiri-ui-toolkit
# Copyright (c) 2014 Rosetta Contributors and Canonical Ltd 2014
# This file is distributed under the same license as the lomiri-ui-toolkit package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: lomiri-ui-toolkit\n"
"Report-Msgid-Bugs-To: FULL NAME <EMAIL@ADDRESS>\n"
"POT-Creation-Date: 2015-11-05 10:05+0100\n"
"PO-Revision-Date: 2014-10-12 19:03+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Czech <cs@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Launchpad-Export-Date: 2017-04-05 07:13+0000\n"
"X-Generator: Launchpad (build 18335)\n"

#: Lomiri/Components/1.2/TextInputPopover.qml:29
#: Lomiri/Components/1.3/TextInputPopover.qml:29
msgid "Select All"
msgstr "Vybrat vše"

#: Lomiri/Components/1.2/TextInputPopover.qml:36
#: Lomiri/Components/1.3/TextInputPopover.qml:36
msgid "Cut"
msgstr "Vyjmout"

#: Lomiri/Components/1.2/TextInputPopover.qml:48
#: Lomiri/Components/1.3/TextInputPopover.qml:48
msgid "Copy"
msgstr "Kopírovat"

#: Lomiri/Components/1.2/TextInputPopover.qml:57
#: Lomiri/Components/1.3/TextInputPopover.qml:57
msgid "Paste"
msgstr "Vložit"

#: Lomiri/Components/1.2/ToolbarItems.qml:143
#: Lomiri/Components/1.3/ToolbarItems.qml:143
msgid "Back"
msgstr "Zpět"

#: Lomiri/Components/ListItems/1.2/Empty.qml:398
#: Lomiri/Components/ListItems/1.3/Empty.qml:398
msgid "Delete"
msgstr "Odstranit"

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:51
msgid "No service/path specified"
msgstr "Žádná služba ani cesta nebyla zadána."

#: Lomiri/Components/plugin/adapters/dbuspropertywatcher_p.cpp:69
#, qt-format
msgid "Invalid bus type: %1."
msgstr "Neplatný typ sběrnice: %1."

#. TRANSLATORS: Time based "this is happening/happened now"
#: Lomiri/Components/plugin/i18n.cpp:268
msgid "Now"
msgstr "Nyní"

#: Lomiri/Components/plugin/i18n.cpp:275
#, qt-format
msgid "%1 minute ago"
msgid_plural "%1 minutes ago"
msgstr[0] "Před %1 minutou"
msgstr[1] "Před %1 minutami"
msgstr[2] "Před %1 minutami"

#: Lomiri/Components/plugin/i18n.cpp:277
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuta"
msgstr[1] "%1 minuty"
msgstr[2] "%1 minut"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:284
msgid "h:mm ap"
msgstr "HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:287
msgid "HH:mm"
msgstr "HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:293
msgid "'Yesterday 'h:mm ap"
msgstr "'Včera 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:296
msgid "'Yesterday 'HH:mm"
msgstr "'Včera  'HHmm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:302
msgid "'Tomorrow 'h:mm ap"
msgstr "'Zítra 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:305
msgid "'Tomorrow 'HH:mm"
msgstr "'Zítra 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:312
msgid "ddd' 'h:mm ap"
msgstr "ddd' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:315
msgid "ddd' 'HH:mm"
msgstr "ddd' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:322
msgid "ddd d MMM' 'h:mm ap"
msgstr "ddd d MMM' 'HH:mm"

#. TRANSLATORS: Please translated these to your locale datetime format using the format specified by
#. https://qt-project.org/doc/qt-5-snapshot/qdatetime.html#fromString-2
#: Lomiri/Components/plugin/i18n.cpp:325
msgid "ddd d MMM' 'HH:mm"
msgstr "ddd d MMM' 'HH:mm"

#: Lomiri/Components/plugin/privates/listitemdragarea.cpp:122
msgid ""
"ListView has no ViewItems.dragUpdated() signal handler implemented. No "
"dragging will be possible."
msgstr ""
"ListView nemá implementovanou funkci ViewItems.dragUpdated(). Přetahávání "
"není dostupné."

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:176
#, qt-format
msgid ""
"property \"%1\" of object %2 has type %3 and cannot be set to value \"%4\" "
"of type %5"
msgstr ""
"vlastnost \"%1\" objektu %2 je typu %3 a nelze proto nastavit jako \"%4\" "
"typu %5"

#: Lomiri/Components/plugin/statesaverbackend_p.cpp:185
#, qt-format
msgid "property \"%1\" does not exist or is not writable for object %2"
msgstr "vlastnost \"%1\"neexistuje a nelze ji proto zapsat pro objekt %2"

#: Lomiri/Components/plugin/ucalarm.cpp:41
#: Lomiri/Components/plugin/ucalarm.cpp:643
msgid "Alarm"
msgstr "Budík"

#: Lomiri/Components/plugin/ucalarm.cpp:635
#: Lomiri/Components/plugin/ucalarm.cpp:667
msgid "Alarm has a pending operation."
msgstr "Máte čekající upomínku."

#: Lomiri/Components/plugin/ucarguments.cpp:188
msgid "Usage: "
msgstr "Použití: "

#: Lomiri/Components/plugin/ucarguments.cpp:209
msgid "Options:"
msgstr "Možnosti:"

#: Lomiri/Components/plugin/ucarguments.cpp:498
#, qt-format
msgid "%1 is expecting an additional argument: %2"
msgstr "%1 očekává další parametr: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:503
#, qt-format
msgid "%1 is expecting a value for argument: %2"
msgstr "%1 očekává hodnotu parametru: %2"

#: Lomiri/Components/plugin/ucarguments.cpp:520
#, qt-format
msgid "%1 is expecting additional arguments: %2"
msgstr "%1 očekává další parametry: %2"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:145
msgid "consider overriding swipeEvent() slot!"
msgstr "consider overriding swipeEvent() slot!"

#: Lomiri/Components/plugin/uclistitemstyle.cpp:165
msgid "consider overriding rebound() slot!"
msgstr "consider overriding rebound() slot!"

#: Lomiri/Components/plugin/ucmousefilters.cpp:1065
msgid "Ignoring AfterItem priority for InverseMouse filters."
msgstr "Priorita AfterItem je ignorována pro InverseMouse filtry."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:77
msgid "Changing connection parameters forbidden."
msgstr "Změna parametrů připojení není možná."

#: Lomiri/Components/plugin/ucserviceproperties.cpp:160
#, qt-format
msgid ""
"Binding detected on property '%1' will be removed by the service updates."
msgstr ""

#: Lomiri/Components/plugin/ucstatesaver.cpp:46
msgid "Warning: attachee must have an ID. State will not be saved."
msgstr ""

#: Lomiri/Components/plugin/ucstatesaver.cpp:56
#, qt-format
msgid ""
"Warning: attachee's UUID is already registered, state won't be saved: %1"
msgstr ""

#: Lomiri/Components/plugin/ucstatesaver.cpp:107
#, qt-format
msgid ""
"All the parents must have an id.\n"
"State saving disabled for %1, class %2"
msgstr ""
"Všechny rodiče musí mít id.\n"
"Ukládání pro %1, třídy %2 zamítnuto"

#: Lomiri/Components/plugin/uctheme.cpp:208
#, qt-format
msgid "Theme not found: \"%1\""
msgstr "Vzhled: \"%1\" se nepodařilo najít"

#: Lomiri/Components/plugin/uctheme.cpp:539
msgid "Not a Palette component."
msgstr "Není prvek palety"

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:462
msgid "Dragging mode requires ListView"
msgstr "Přetahávání vyžaduje ListView"

#: Lomiri/Components/plugin/ucviewitemsattached.cpp:468
msgid ""
"Dragging is only supported when using a QAbstractItemModel, ListModel or "
"list."
msgstr ""
"Přetahávání je dostupné pouze při použití QAbstractItemModel, ListModel nebo "
"list."

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:78
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:78
msgid "Cancel"
msgstr "Zrušit"

#: Lomiri/Components/Popups/1.2/ComposerSheet.qml:88
#: Lomiri/Components/Popups/1.3/ComposerSheet.qml:88
msgid "Confirm"
msgstr "Potvrdit"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:85
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:85
msgid "Close"
msgstr "Zavřít"

#: Lomiri/Components/Popups/1.2/DefaultSheet.qml:95
#: Lomiri/Components/Popups/1.3/DefaultSheet.qml:95
msgid "Done"
msgstr "Hotovo"

#: Lomiri/Components/Themes/Ambiance/1.2/ProgressBarStyle.qml:51
#: Lomiri/Components/Themes/Ambiance/1.3/ProgressBarStyle.qml:50
msgid "In Progress"
msgstr "Probíhá"

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Release to refresh..."
msgstr "Uvolnit pro obnovení..."

#: Lomiri/Components/Themes/Ambiance/1.2/PullToRefreshStyle.qml:28
#: Lomiri/Components/Themes/Ambiance/1.3/PullToRefreshStyle.qml:28
msgid "Pull to refresh..."
msgstr "Přetáhnouot pro obnovení..."

#~ msgid "Theme not found: "
#~ msgstr "Motiv nebyl nalezen: "
